# Shopify Theme Developer Boilerplate
Simple boilerplate setup for developing Shopify Themes.
[Youfront/shopify-development-boilerplate](https://github.com/Youfront/shopify-development-boilerplate)

#### Commands
- `$ npm run production`<br>Build CSS and JS assets for production environment.

- `$ npm run dev`<br>Build CSS and JS assets for development environment.

- `$ npm run watch`<br>Watch for changes in CSS and JS assets, and build them for development environment.

- `$ npm run lint`<br>Run ESLint on JavaScript resources.

- `$ theme watch`<br>Watch for file changes, and upload them to Shopify theme.

- `$ theme deploy`<br>Deploy all files to Shopify theme.

#### Stack
- [Themekit](https://github.com/Shopify/themekit)
- [Webpack](https://webpack.js.org/)
- [PostCSS](https://postcss.org/)
- [ESLint](https://eslint.org/) (Using AirBnb styleguide)
