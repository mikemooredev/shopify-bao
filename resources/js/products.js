// ajax process add-to-bag links
document.querySelectorAll("a.btn--add-to-bag").forEach(item => {
    item.addEventListener("click", function(e){
        
        e.preventDefault();

        ajax.request(
            'get',
            e.target.getAttribute("href")
        ).then(data => {
            // log to debug
            //console.log(data);
            return ajax.updateBag(data);
        }).catch(error => {
            // log to debug
            console.log(error)
        })

        return false;

    });
})

const ajax = {

    request: (methodType, url) => {

        return new Promise((resolve, reject) => {
            var xhr = new XMLHttpRequest();
            xhr.open(methodType, url, true);
            xhr.onreadystatechange = function(){
                if (xhr.readyState === 4 && xhr.status === 200){                               
                    resolve(xhr.response)
                }
                if (xhr.status != 200){                               
                    reject(xhr.response)
                }
            }
            xhr.send();
        });
    },

    updateBag: (data) => {

        const doc = new DOMParser().parseFromString(data, "text/html");
        const newCartLink = doc.getElementById('cart-link');
        document.getElementById('cart-link').replaceWith(newCartLink);
    
        // inform customer product added to bag 
        // replace with pretty modal
        alert("Product added to bag!");
    
    }
}